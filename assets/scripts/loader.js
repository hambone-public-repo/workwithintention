var Loader = function() {

    var ready = false;

    window.addEventListener('WebComponentsReady', function(e) {
        evaluateLoading();
        
    });

    document.querySelector("#pre").addEventListener("animationend", function(e) {
        evaluateLoading();
        document.querySelector("#plus").className = 'plus pulse';
    });

    function evaluateLoading() {
        if (ready){
            document.querySelector("#loader").className += ' anim';
            document.getElementById("app").applyScrollBar();
        }
        else
            ready = true;
    }
};

var load = new Loader();